package com.example.desafio.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.example.desafio.entity.Avistamento;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AvistamentoDTO implements Serializable{

    private Long id;
    private AveDTO ave;
    private UsuarioDTO usuario;
    private LocalDateTime createdAt;
    private String localizacao;


     public AvistamentoDTO(Avistamento entity) {
        this.id = entity.getId();
        this.createdAt = entity.getCreatedAt();
        this.localizacao = entity.getLocalizacao();
        this.ave = new AveDTO(entity.getAve());
        this.usuario = new UsuarioDTO(entity.getUsuario());
    }
}