package com.example.desafio.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;

import com.example.desafio.entity.Usuario;

public class UsuarioDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @Email(message = "Favor entrar um email válido")
    private String email;
    private String password;

    Set<RoleDTO> roles = new HashSet<>();

    public UsuarioDTO() {
    }

    public UsuarioDTO(Long id) {
        this.id = id;
    }

    public UsuarioDTO(Long id, String email) {
        this.id = id;
        this.email = email;
    }



    public UsuarioDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UsuarioDTO(Long id, String email, String password, Set<RoleDTO> roles) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public UsuarioDTO(Long id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }


    public UsuarioDTO(Usuario entity) {
        id = entity.getId();
        email = entity.getEmail();
        password = entity.getPassword();
        entity.getRoles().forEach(role -> this.roles.add(new RoleDTO(role)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }
}