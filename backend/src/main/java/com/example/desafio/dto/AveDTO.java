package com.example.desafio.dto;

import com.example.desafio.entity.Ave;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AveDTO {
    private Long id;
    private String nome;
    private String tamanho;
    private String genero;
    private String corPredominante;
    private String familia;
    private String habitat;
    private int pagina;
    private int linha;
    private int coluna;

    public AveDTO(Ave entity) {
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.tamanho = entity.getTamanho();
        this.genero = entity.getGenero();
        this.corPredominante = entity.getCorPredominante();
        this.familia = entity.getFamilia();
        this.habitat = entity.getHabitat();
        this.pagina = entity.getPagina();
        this.linha = entity.getLinha();
        this.coluna = entity.getColuna();
    }
}