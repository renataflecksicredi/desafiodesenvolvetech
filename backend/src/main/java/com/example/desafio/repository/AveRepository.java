package com.example.desafio.repository;

import com.example.desafio.entity.Ave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AveRepository extends JpaRepository<Ave, Long> {
}