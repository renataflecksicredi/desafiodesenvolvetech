package com.example.desafio.repository;

import com.example.desafio.entity.Avistamento;
import com.example.desafio.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvistamentoRepository extends JpaRepository <Avistamento, Long> {
    Page<Avistamento> findByUsuario(Usuario usuario, Pageable pageable);
}