package com.example.desafio.controller;

import com.example.desafio.dto.AvistamentoDTO;
import com.example.desafio.service.AvistamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value ="/avistamentos")
public class AvistamentoController {

    @Autowired
    private AvistamentoService service;

    @GetMapping
    public ResponseEntity<Page<AvistamentoDTO>> findAllPaged(Pageable pageable) {
        Page<AvistamentoDTO> page = service.findAllPaged(pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AvistamentoDTO> findById(@PathVariable Long id){
        AvistamentoDTO avistamentoDTO = service.findById(id);
        return ResponseEntity.ok().body(avistamentoDTO);
    }

    @PostMapping
    public ResponseEntity<AvistamentoDTO> insert(@RequestBody AvistamentoDTO avistamentoDTO) {
        avistamentoDTO = service.insert(avistamentoDTO);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/id}")
                .buildAndExpand(avistamentoDTO.getId()).toUri();
        return ResponseEntity.created(uri).body(avistamentoDTO);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<AvistamentoDTO> update(@PathVariable Long id, @RequestBody AvistamentoDTO avistamentoDTO){
        avistamentoDTO = service.update(id, avistamentoDTO);
        return ResponseEntity.ok().body(avistamentoDTO);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<AvistamentoDTO> delete(@PathVariable Long id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

}

