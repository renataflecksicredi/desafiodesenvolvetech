package com.example.desafio.controller;

import com.example.desafio.dto.AveDTO;
import com.example.desafio.service.AveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/aves")
public class AveController {
    @Autowired
    private AveService service;

    @GetMapping
    public ResponseEntity<Page<AveDTO>> findAll(Pageable pageable){
        Page<AveDTO> list = service.findAllPaged(pageable);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AveDTO> findById(@PathVariable Long id){
        AveDTO aveDTO = service.findById(id);
        return ResponseEntity.ok().body(aveDTO);
    }

    @PostMapping
    public ResponseEntity<AveDTO> insert(@RequestBody AveDTO aveDTO){
        aveDTO = service.insert(aveDTO);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(aveDTO.getId())
                .toUri();
        return ResponseEntity.created(uri).body(aveDTO);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<AveDTO> update(@PathVariable Long id, @RequestBody AveDTO aveDTO){
        aveDTO = service.update(id, aveDTO);
        return ResponseEntity.ok().body(aveDTO);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<AveDTO> delete(@PathVariable Long id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}