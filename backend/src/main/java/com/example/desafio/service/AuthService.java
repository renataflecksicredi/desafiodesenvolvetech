package com.example.desafio.service;

import com.example.desafio.entity.Usuario;
import com.example.desafio.repository.UsuarioRepository;
import com.example.desafio.service.exception.ForbiddenException;
import com.example.desafio.service.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthService {

    @Autowired
    private UsuarioRepository userRepository;

    @Transactional(readOnly = true)
    public Usuario authenticated() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            return userRepository.findByEmail(username);
        }
        catch (Exception e) {
            throw new UnauthorizedException("Invalid user");
        }
    }

    public void validateSelfOrAdmin(Long userId) {
        Usuario usuario = authenticated();
        if (!usuario.getId().equals(userId) && !usuario.hasHole("ROLE_ADMIN")) {
            throw new ForbiddenException("Access denied");
        }
    }
}
