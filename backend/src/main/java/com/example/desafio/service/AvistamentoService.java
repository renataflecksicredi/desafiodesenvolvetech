package com.example.desafio.service;

import com.example.desafio.dto.AvistamentoDTO;
import com.example.desafio.entity.Ave;
import com.example.desafio.entity.Avistamento;
import com.example.desafio.entity.Usuario;
import com.example.desafio.repository.AveRepository;
import com.example.desafio.repository.AvistamentoRepository;
import com.example.desafio.repository.UsuarioRepository;
import com.example.desafio.service.exception.DatabaseException;
import com.example.desafio.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;


@Service
public class AvistamentoService {

    @Autowired
    private AvistamentoRepository repository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private AveRepository aveRepository;

    @Autowired
    private AuthService authService;

    @Transactional(readOnly = true)
    public Page<AvistamentoDTO> findAllPaged(Pageable pageable){
        Usuario usuario = authService.authenticated();
        Page<Avistamento> page = repository.findByUsuario(usuario, pageable);
        return page.map(AvistamentoDTO::new);
    }

    @Transactional(readOnly = true)
    public AvistamentoDTO findById(Long id) {
        Optional<Avistamento> obj = repository.findById(id);
        Avistamento entity = obj.orElseThrow(()-> new ResourceNotFoundException("Avistamento não encontrado"));
        return new AvistamentoDTO(entity);
    }

    @Transactional
    public AvistamentoDTO insert(AvistamentoDTO avistamento) {
        Avistamento entity = new Avistamento();
        copyDtoToEntity(avistamento, entity);
        entity = repository.save(entity);
        return new AvistamentoDTO(entity);
    }

    @Transactional
    public AvistamentoDTO update(Long id, AvistamentoDTO avistamentoDTO) {
        try{
            Avistamento entity = repository.getReferenceById(id);
            copyDtoToEntity(avistamentoDTO, entity);
            entity = repository.save(entity);
            return new AvistamentoDTO(entity);
        }catch (EntityNotFoundException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        }catch (EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }catch (DataIntegrityViolationException e){
            throw new DatabaseException("Integrity violation");
        }
    }

    private void copyDtoToEntity(AvistamentoDTO avistamentoDTO, Avistamento entity) {
        entity.setLocalizacao(avistamentoDTO.getLocalizacao());
        entity.setCreatedAt(avistamentoDTO.getCreatedAt());

        Long idAve = avistamentoDTO.getAve().getId();
        Long idUsuario = avistamentoDTO.getUsuario().getId();

        Ave ave = aveRepository.getReferenceById(idAve);
        Usuario usuario = usuarioRepository.getReferenceById(idUsuario);

        entity.setAve(ave);
        entity.setUsuario(usuario);
    }
}