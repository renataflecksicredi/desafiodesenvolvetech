package com.example.desafio.service;

import com.example.desafio.dto.AveDTO;
import com.example.desafio.entity.Ave;
import com.example.desafio.repository.AveRepository;
import com.example.desafio.service.exception.DatabaseException;
import com.example.desafio.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AveService {
    @Autowired
    private AveRepository repository;

    @Transactional(readOnly = true)
    public Page<AveDTO> findAllPaged(Pageable pageable) {
        Page<Ave> list = repository.findAll(pageable);
        return list.map(AveDTO::new);
    }

    @Transactional(readOnly = true)
    public AveDTO findById(Long id) {
        Optional<Ave> obj = repository.findById(id);
        Ave entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));
        return new AveDTO(entity);
    }

    @Transactional
    public AveDTO insert(AveDTO aveDTO) {
        Ave entity = new Ave();
        copyDtoToEntity(aveDTO, entity);
        entity = repository.save(entity);
        return new AveDTO(entity);
    }

    @Transactional
    public AveDTO update(Long id, AveDTO aveDTO) {
        try{
            Ave entity = repository.getReferenceById(id);
            copyDtoToEntity(aveDTO, entity);
            entity = repository.save(entity);
            return new AveDTO(entity);
        }catch (EntityNotFoundException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        }catch (EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }catch (DataIntegrityViolationException e){
            throw new DatabaseException("Integrity violation");
        }
    }

    private void copyDtoToEntity(AveDTO aveDTO, Ave entity) {
        entity.setNome(aveDTO.getNome());
        entity.setTamanho(aveDTO.getTamanho());
        entity.setGenero(aveDTO.getGenero());
        entity.setCorPredominante(aveDTO.getCorPredominante());
        entity.setFamilia(aveDTO.getFamilia());
        entity.setHabitat(aveDTO.getHabitat());
        entity.setPagina(aveDTO.getPagina());
        entity.setLinha(aveDTO.getLinha());
        entity.setColuna(aveDTO.getColuna());
    }
}