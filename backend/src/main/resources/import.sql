INSERT INTO tb_user (email, password) VALUES ('alex@gmail.com', '$2a$10$eACCYoNOHEqXve8aIWT8Nu3PkMXWBaOxJ9aORUYzfMQCbVBIhZ8tG');
INSERT INTO tb_user (email, password) VALUES ('maria@gmail.com', '$2a$10$eACCYoNOHEqXve8aIWT8Nu3PkMXWBaOxJ9aORUYzfMQCbVBIhZ8tG');

INSERT INTO tb_role (authority) VALUES ('ROLE_OPERATOR');
INSERT INTO tb_role (authority) VALUES ('ROLE_ADMIN');

INSERT INTO tb_user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO tb_user_role (user_id, role_id) VALUES (2, 2);

INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('João-grande', 'Grande', 'Femea', 'Branco', 'Ciconiidae', 'Campo alagado', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Garça-vaqueira', 'Médio', 'Femea', 'Branco', 'Ardeidae', 'Campo seco baixo', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Gavião-peneira', 'Médio', 'Macho', 'Cinza', 'Accipitridae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Gaivota-maria-velha', 'Médio', 'Macho', 'Branco', 'Laridae', 'Banhado', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Urubu-de-cabeça-vermelha', 'Grande', 'Femea', 'Preto', 'Cathartidae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Gavião-caramujeiro', 'Médio', 'Femea', 'Preto', 'Accipitridae', 'Banhado com vegetação alta', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Chopim-do-brejo', 'Pequeno', 'Macho', 'Amarelo', 'Icteridae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Dragão', 'Pequeno', 'Macho', 'Marrom', 'Icteridae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Príncipe', 'Pequeno', 'Macho', 'Vermelho', 'Tyrannidae', 'Campo seco baixo', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Andorinha-pequena-de-casa', 'Pequeno', 'Femea', 'Azul', 'Hirundinidae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Garça-moura', 'Grande', 'Macho', 'Cinza', 'Ardeidae', 'Campo alagado', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Maçarico-real', 'Grande', 'Femea', 'Marrom','Threskiornithidae', 'Banhado', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Gavião-cinza', 'Médio', 'Macho', 'Cinza', 'Accipitridae', 'Banhado com vegetação alta', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Pombão', 'Médio', 'Femea', 'Marrom', 'Columbidae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('João-pobre', 'Pequeno', 'Femea', 'Marrom', 'Tyrannidae', 'Banhado com vegetação alta', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Balança-rabo-de-máscara', 'Pequeno', 'Macho', 'Cinza', 'Polioptilidae', 'Campo com árvores', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Patativa-tropeira', 'Pequeno', 'Macho', 'Azul', 'Thraupidae', 'Campo seco alto', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Socó-boi', 'Grande', 'Femea', 'Marrom', 'Ardeidae', 'Campo alagado', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Perdigão', 'Médio', 'Macho', 'Marrom', 'Tinamidae', 'Campo seco alto', 8, 1, 1);
INSERT INTO ave (nome, tamanho, genero, cor_Predominante, familia, habitat, pagina, linha, coluna) VALUES ('Caturrita', 'Pequeno', 'Macho', 'Verde', 'Psittacidae', 'Campo seco baixo', 8, 1, 1);

INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (1, 1, '2022-11-10 16:38:11.971686', 'Porto Alegre');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (3, 1, '2022-11-11 16:38:11.971686', 'Canoas');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (5, 1, '2022-11-12 16:38:11.971686', 'Gravataí');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (7, 1, '2022-11-13 16:38:11.971686', 'Porto Alegre');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (9, 1, '2022-11-14 16:38:11.971686', 'Porto Alegre');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (11, 2, '2022-11-15 16:38:11.971686', 'Gravataí');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (13, 2, '2022-11-16 16:38:11.971686', 'Porto Alegre');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (15, 2, '2022-11-17 16:38:11.971686', 'Alvorada');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (17, 2, '2022-11-18 16:38:11.971686', 'Porto Alegre');
INSERT INTO avistamentos (ave_id, usuario_id, created_At, localizacao) VALUES (19, 2, '2022-11-19 16:38:11.971686', 'Canoas');