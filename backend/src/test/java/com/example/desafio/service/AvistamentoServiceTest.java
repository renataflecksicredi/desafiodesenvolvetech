package com.example.desafio.service;

import com.example.desafio.dto.AvistamentoDTO;
import com.example.desafio.entity.Ave;
import com.example.desafio.entity.Avistamento;
import com.example.desafio.entity.Usuario;
import com.example.desafio.repository.AveRepository;
import com.example.desafio.repository.AvistamentoRepository;
import com.example.desafio.repository.UsuarioRepository;
import com.example.desafio.service.exception.ResourceNotFoundException;
import com.example.desafio.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
public class AvistamentoServiceTest {
    private Long existingId;
    private Long nonExistingId;

    private PageImpl<Avistamento> page;
    private AvistamentoDTO avistamentoDTO;
    private Avistamento avistamento;
    private Ave ave;
    private Usuario usuario;

    @InjectMocks
    private AvistamentoService service;

    @Mock
    private AvistamentoRepository repository;

    @Mock
    private UsuarioRepository usuarioRepository;

    @Mock
    private AveRepository  aveRepository;

    @Mock
    private AuthService authService;

    @BeforeEach
    void setUp() throws Exception{
        existingId = 1L;
        nonExistingId = 80L;
        avistamentoDTO = Factory.createAvistamentoDTO();
        avistamento = Factory.createAvistamento();
        page = new PageImpl<>(List.of(avistamento));
        ave = Factory.createAve();
        usuario = Factory.createUsuario();

        //Quando o método retorna eu primeiro digo qual ação deve ser realizada e depois digo quando ela deve ser realizada
        //Quando os dados não importam, apenas o tipo de retorno, posso usar a classe ArgumentMatchers que retorna um objeto que o método precisa
        //No entanto quando o método te sobrecarga com tipos diferentes de argumento, eu preciso especificar qual tipo eu preciso, fazendo casting
        Mockito.when(repository.findAll((Pageable) ArgumentMatchers.any())).thenReturn(page);

        Mockito.when(repository.save(ArgumentMatchers.any())).thenReturn(avistamento);

        Mockito.when(repository.findById(existingId)).thenReturn(Optional.of(avistamento));
        Mockito.when(repository.findById(nonExistingId)).thenReturn(Optional.empty());

        Mockito.when(repository.getReferenceById(existingId)).thenReturn(avistamento);
        Mockito.when(repository.getReferenceById(nonExistingId)).thenThrow(EntityNotFoundException.class);

        Mockito.when(aveRepository.getReferenceById(existingId)).thenReturn(ave);
        Mockito.when(aveRepository.getReferenceById(nonExistingId)).thenThrow(EntityNotFoundException.class);

        Mockito.when(usuarioRepository.getReferenceById(existingId)).thenReturn(usuario);
        Mockito.when(usuarioRepository.getReferenceById(nonExistingId)).thenThrow(EntityNotFoundException.class);

        //Quando o método é void eu primeiro digo qual deve ser a ação e depois digo quando essa ação deve ser realizada
        //doNothing usa-se quando o método não deve fazer nada
        Mockito.doNothing().when(repository).deleteById(existingId);
        Mockito.doThrow(EmptyResultDataAccessException.class).when(repository).deleteById(nonExistingId);

        Mockito.when(authService.authenticated()).thenReturn(usuario);
    }

    @Test
    public void findByIdShouldReturnAvistamentoDtoWhenIdExists(){
        AvistamentoDTO result = service.findById(existingId);
        Assertions.assertNotNull(result);
    }

    @Test
    public void findByIdShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.findById(nonExistingId);
        });
    }

    @Test
    public void updateShouldReturnAvistamentoDtoWhenIdExists(){
        AvistamentoDTO result = service.update(existingId, avistamentoDTO);
        Assertions.assertNotNull(result);
    }

    @Test
    public void updateShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.update(nonExistingId, avistamentoDTO);
        });
    }

    @Test
    public void deleteShouldDoNothingWhenIdExists(){
        Assertions.assertDoesNotThrow(() -> {
            service.delete(existingId);
        });
        Mockito.verify(repository, Mockito.times(1)).deleteById(existingId);
    }

    @Test
    public void deleteShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.delete(nonExistingId);
        });
        Mockito.verify(repository, Mockito.times(1)).deleteById(nonExistingId);
    }
}