package com.example.desafio.service;

import com.example.desafio.dto.UsuarioDTO;
import com.example.desafio.entity.Role;
import com.example.desafio.entity.Usuario;
import com.example.desafio.repository.RoleRepository;
import com.example.desafio.repository.UsuarioRepository;
import com.example.desafio.service.exception.DatabaseException;
import com.example.desafio.service.exception.ResourceNotFoundException;
import com.example.desafio.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class) //não carrega o contexto da aplicação, mas permite usar o spring com JUnit
public class UsuarioServiceTests {

    @InjectMocks
    private UsuarioService usuarioService;

    @Mock //config o mock
    private UsuarioRepository usuarioRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    private long existingId;
    private long nonExistingId;
    private long dependentId;
    private PageImpl<Usuario> page; //representa uma página de dados do usuário
    private Usuario usuario;
    private UsuarioDTO usuarioDTO;
    private Role role;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 4L;
        dependentId = 2L;
        usuario = Factory.createUsuario();
        usuarioDTO = Factory.createUsuarioDTO();
        role = Factory.createRole();
        page = new PageImpl<>(List.of(usuario));

        Mockito.when(usuarioRepository.findAll((Pageable)ArgumentMatchers.any())).thenReturn(page);

        Mockito.when(usuarioRepository.save(ArgumentMatchers.any())).thenReturn(usuario);

        Mockito.when(usuarioRepository.findById(existingId)).thenReturn(Optional.of(usuario));
        Mockito.when(usuarioRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        Mockito.when(usuarioRepository.getReferenceById(existingId)).thenReturn(usuario);
        Mockito.when(usuarioRepository.getReferenceById(nonExistingId)).thenThrow(EntityNotFoundException.class);
        Mockito.when(roleRepository.getReferenceById(existingId)).thenReturn(role);

        //chama método encode com a senha e retorna a senha do usuarioDTO
        Mockito.when(passwordEncoder.encode(usuarioDTO.getPassword())).thenReturn(usuarioDTO.getPassword());

        Mockito.doNothing().when(usuarioRepository).deleteById(existingId);
        Mockito.doThrow(EmptyResultDataAccessException.class).when(usuarioRepository).deleteById(nonExistingId);
        Mockito.doThrow(DataIntegrityViolationException.class).when(usuarioRepository).deleteById(dependentId);
    }

    @Test
    public void findAllPagedShouldReturnPage(){
        Pageable pageable = PageRequest.of(0, 10);
        Page<UsuarioDTO> result = usuarioService.findAllPaged(pageable);
        Assertions.assertNotNull(result);
        //verifica se o findAll(pageable) do repository foi chamado lá dentro do service
        Mockito.verify(usuarioRepository).findAll(pageable);
    }

    @Test
    public void findByIdShouldReturnUsuarioDTOWhenIdExists(){
        UsuarioDTO result = usuarioService.findById(existingId);
        Assertions.assertNotNull(result);
    }

    @Test
    public void findByIdShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            usuarioService.findById(nonExistingId);
        });
    }

    @Test
    public void updateShouldReturnUsuarioDTOWhenIdExists(){
        UsuarioDTO result = usuarioService.update(existingId, usuarioDTO);
        Assertions.assertNotNull(result);
    }

    @Test
    public void updateShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            usuarioService.update(nonExistingId, usuarioDTO);
        });
    }

    @Test
    public void deleteShouldDoNothingWhenIdExists(){
        Assertions.assertDoesNotThrow(() -> {
            usuarioService.delete(existingId);
        });
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(existingId);
    }

    @Test
    public void deleteShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            usuarioService.delete(nonExistingId);
        });
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(nonExistingId);
    }

    @Test
    public void deleteShouldThrowDatabaseExceptionWhenDependentId(){
        Assertions.assertThrows(DatabaseException.class, () -> {
            usuarioService.delete(dependentId);
        });
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(dependentId);
    }
}
