//package com.example.desafio.service;
//
//import com.example.desafio.dto.UsuarioDTO;
//import com.example.desafio.repository.UsuarioRepository;
//import com.example.desafio.service.exception.ResourceNotFoundException;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//
//@SpringBootTest //carrega contexto da aplicação
//public class UsuarioServiceIT {
//
//    @Autowired
//    private UsuarioService usuarioService;
//
//    @Autowired
//    private UsuarioRepository usuarioRepository;
//
//    private Long existingId;
//    private Long nonExistingId;
//    private Long countTotalUsuarios;
//
//    @BeforeEach
//    void setUp() throws Exception {
//        existingId = 2L;
//        nonExistingId = 4L;
//        countTotalUsuarios = 2L;
//    }
//
//    @Test
//    public void findAllPagedShouldReturnPagedWhenPage0Size10(){
//        PageRequest pageRequest = PageRequest.of(0, 10);
//        Page<UsuarioDTO> result = usuarioService.findAllPaged(pageRequest);
//        Assertions.assertFalse(result.isEmpty());
//    }
//
//    @Test
//    public void deleteShouldDeleteResourceWhenIdExists(){
//        usuarioService.delete(existingId);
//        Assertions.assertEquals(countTotalUsuarios-1, usuarioRepository.count());
//    }
//
//    @Test
//    public void deleteShouldThrowResourceNotFoundExceptionWhenIdDoesNotExist(){
//        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
//            usuarioService.delete(nonExistingId);
//        });
//    }
//}
