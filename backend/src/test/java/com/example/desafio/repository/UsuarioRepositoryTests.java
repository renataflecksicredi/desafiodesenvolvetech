package com.example.desafio.repository;

import com.example.desafio.entity.Usuario;
import com.example.desafio.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Optional;

@DataJpaTest //carrega os componentes relacionado ao Spring Data JPA
public class UsuarioRepositoryTests {
    //usa os dados do import.sql
    @Autowired
    private UsuarioRepository usuarioRepository;

    private long existingId;
    private long nonExistingId;
    private long countTotalUsers;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 4L;
        countTotalUsers = 2L;
    }

    @Test
    public void findByIdShouldReturnNonEmptyOptionalWhenIdExists(){
        Optional<Usuario> result = usuarioRepository.findById(existingId);
        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    public void findByIdShouldReturnEmptyOptionalWhenIdDoesNotExist(){
        Optional<Usuario> result = usuarioRepository.findById(nonExistingId);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void saveShouldPersistWithAutoincrementWhenIdIsNull(){
        Usuario usuario = Factory.createUsuario();
        usuario.setId(null);
        usuario = usuarioRepository.save(usuario);
        Assertions.assertNotNull(usuario.getId());
        Assertions.assertEquals(countTotalUsers + 1, usuario.getId());
    }

    @Test
    public void deleteShouldDeleteObjectWhenIdExists(){
        usuarioRepository.deleteById(existingId);
        Optional<Usuario> result = usuarioRepository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    public void deleteShouldThrowEmptyResultDataAccessExceptionWhenIdDoesNotExist(){
        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            usuarioRepository.deleteById(nonExistingId);
        });
    }
}
