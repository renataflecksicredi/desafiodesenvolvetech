package com.example.desafio.repository;


import com.example.desafio.entity.Avistamento;
import com.example.desafio.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Optional;

@DataJpaTest
public class AvistamentoRepositoryTest {
    @Autowired
    private AvistamentoRepository repository;

    private Long existentId;
    private Long nonExistentId;
    private Long countTotalAvistamentos;

    @BeforeEach
    void setUp() throws Exception{
        existentId = 1L;
        nonExistentId = 100L;
        countTotalAvistamentos = 10L;
    }

    @Test
    public void findByIdShouldReturnNonEmptyAvistamentoOptionalWhenIdExists(){
        Optional<Avistamento> findById = repository.findById(existentId);
        Assertions.assertFalse(findById.isEmpty());
    }

    @Test
    public void findByIdShouldReturnEmptyAvistamentoOptionalWhenIdDoesNotExist(){
        Optional<Avistamento> findById = repository.findById(nonExistentId);
        Assertions.assertTrue(findById.isEmpty());
    }

    @Test
    public void saveShouldPersistWithAutoincrementWhenIdIsNull(){
        Avistamento avistamento = Factory.createAvistamento();
        avistamento.setId(null);
        avistamento = repository.save(avistamento);
        Assertions.assertNotNull(avistamento.getId());
        Assertions.assertEquals(countTotalAvistamentos + 1, avistamento.getId());
    }

    @Test
    public void deleteShouldDeleteObjectWhenIdExists(){
        repository.deleteById(existentId);
        Optional<Avistamento> result = repository.findById(existentId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    public void deleteShouldThrowEmptyResultDataAccessExceptionWhenIdDoesNotExist() {
        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            repository.deleteById(nonExistentId);
        });
    }
}