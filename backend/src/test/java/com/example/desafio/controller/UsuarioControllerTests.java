package com.example.desafio.controller;

import com.example.desafio.dto.UsuarioDTO;
import com.example.desafio.service.UsuarioService;
import com.example.desafio.service.exception.DatabaseException;
import com.example.desafio.service.exception.ResourceNotFoundException;
import com.example.desafio.tests.Factory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = UsuarioController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class}) //carrega o contexto apenas da camada web
public class UsuarioControllerTests {
    //Mockmvc para simular requisições
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private ObjectMapper objectMapper;

    private Long existingId;
    private Long nonExistingId;
    private Long dependentId;
    private UsuarioDTO usuarioDTO;
    private PageImpl<UsuarioDTO> page;

    @BeforeEach
    void setUp() throws Exception {
        existingId =1L;
        nonExistingId = 4L;
        dependentId = 2L;

        usuarioDTO = Factory.createUsuarioDTO();
        page = new PageImpl<>(List.of(usuarioDTO));

        when(usuarioService.findAllPaged(any())).thenReturn(page);

        when(usuarioService.findById(existingId)).thenReturn(usuarioDTO);
        when(usuarioService.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        when(usuarioService.insert(any())).thenReturn(usuarioDTO);

        when(usuarioService.update(eq(existingId), any())).thenReturn(usuarioDTO);
        when(usuarioService.update(eq(nonExistingId), any())).thenThrow(ResourceNotFoundException.class);

        doNothing().when(usuarioService).delete(existingId);
        doThrow(ResourceNotFoundException.class).when(usuarioService).delete(nonExistingId);
        doThrow(DatabaseException.class).when(usuarioService).delete(dependentId);

    }

    @Test
    public void findAllShouldReturnPage() throws Exception {
        ResultActions result =
                mockMvc.perform(get("/usuarios")
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isOk());
    }

    @Test
    public void findByIdShouldReturnUsuarioWhenIdExists() throws Exception {
        ResultActions result =
                mockMvc.perform(get("/usuarios/{id}", existingId)
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isOk());

        result.andExpect(jsonPath("$.id").exists());
        result.andExpect(jsonPath("$.email").exists());
    }

    @Test
    public void findByIdShouldReturnNotFoundWhenIdDoesNotExist() throws Exception {
        ResultActions result =
                mockMvc.perform(get("/usuarios/{id}", nonExistingId)
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isNotFound());
    }

    @Test
    public void insertShouldReturnUsuarioDTOCreated() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(usuarioDTO);

        ResultActions result =
                mockMvc.perform(post("/usuarios", existingId)
                        .content(jsonBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());

        result.andExpect(jsonPath("$.id").exists());
        result.andExpect(jsonPath("$.email").exists());
    }

    @Test
    public void updateShouldReturnUsuarioDTOWhenIdExists() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(usuarioDTO);

        ResultActions result =
                mockMvc.perform(put("/usuarios/{id}", existingId)
                        .content(jsonBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());

        result.andExpect(jsonPath("$.id").exists());
        result.andExpect(jsonPath("$.email").exists());
    }

    @Test
    public void updateShouldReturnNotFoundWhenIdDoesNotExist() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(usuarioDTO);

        ResultActions result =
                mockMvc.perform(put("/usuarios/{id}", nonExistingId)
                        .content(jsonBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNotFound());
    }

    @Test
    public void deleteShouldReturnNoContentWhenIdExists() throws Exception {
        ResultActions result =
                mockMvc.perform(delete("/usuarios/{id}", existingId)
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isNoContent());
    }

    @Test
    public void deleteShouldReturnNotFoundWhenIdDoesNotExist() throws Exception {
        ResultActions result =
                mockMvc.perform(delete("/usuarios/{id}", nonExistingId)
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isNotFound());
    }

    @Test
    public void deleteShouldReturnBadRequestWhenDependentId() throws Exception {
        ResultActions result =
                mockMvc.perform(delete("/usuarios/{id}", dependentId)
                        .accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isBadRequest());
    }
}