package com.example.desafio.tests;

import com.example.desafio.dto.AveDTO;
import com.example.desafio.dto.AvistamentoDTO;
import com.example.desafio.dto.UsuarioDTO;
import com.example.desafio.entity.Ave;
import com.example.desafio.entity.Avistamento;
import com.example.desafio.entity.Role;
import com.example.desafio.entity.Usuario;

import java.time.LocalDateTime;

public class Factory {

    public static Role createRole(){
        Role role = new Role(1L, "ADMIN");
        return role;
    }

    public static Usuario createUsuario(){
        Usuario usuario = new Usuario(1L, "marcos@teste", "senhatest");
        usuario.getRoles().add(createRole());
        return usuario;
    }

    public static UsuarioDTO createUsuarioDTO(){
        Usuario usuario = createUsuario();
        return new UsuarioDTO(usuario);
    }

    public static Ave createAve (){
        Ave ave = new Ave(1L, "João-de-barro", "médio", "M", "cinza", "familia", "savana", 1, 2, 3);
        return ave;
    }

    public static AveDTO createAveDTO() {
        Ave ave = createAve();
        return new AveDTO(ave);
    }

    public static Avistamento createAvistamento(){
        Ave ave = createAve();
        Usuario usuario = createUsuario();
        Avistamento avistamento = new Avistamento(1L, ave, usuario, LocalDateTime.now(), "POA");
        return avistamento;
    }

    public static AvistamentoDTO createAvistamentoDTO(){
        Avistamento avistamento = createAvistamento();
        return new AvistamentoDTO(avistamento);
    }

  }
