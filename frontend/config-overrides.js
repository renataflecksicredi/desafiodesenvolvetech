module.exports = function override(config, env) {
    config.resolve.fallback = {
        "fs": false,
        "tls": false,
        "net": false,
        "path": false,
        "zlib": false,
        "http": false,
        "https": false,
        "querystring": false
    }
    return config;
}