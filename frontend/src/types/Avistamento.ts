import { Ave } from "./Ave";
import { Usuario } from "./Usuario";

export type Avistamento = {
    id: number;
    ave: Ave;
    usuario: Usuario;
    createdAt: string;
    localizacao: string;
}