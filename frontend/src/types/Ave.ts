export type Ave = {
    id: number;
    nome: string;
    tamanho: string;
    genero: string;
    corPredominante: string;
    familia: string;
    habitat: string;
    pagina: number;
    linha: number;
    coluna: number;
}