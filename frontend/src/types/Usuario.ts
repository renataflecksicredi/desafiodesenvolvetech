export type Usuario = {
    id: number;
    email: string;
    password?: string
}