import axios from "axios";
import querystring from "querystring";
import { Method, AxiosRequestConfig } from "axios";
import { Ave } from "../types/Ave";
import { Avistamento } from "../types/Avistamento";

const apiCredentials = {
  username: "desafiodesenvolvetech",
  password: "desafiodesenvolvetech",
};

export const useApi = () => ({
  signin: (email: string, password: string) => {
    const data = querystring.stringify({
      username: email,
      password: password,
      grant_type: "password",
    });

    const config: AxiosRequestConfig = {
      method: "POST",
      url: "http://localhost:8080/oauth/token",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      auth: apiCredentials,
      data: data,
    };

    const requestResponse: any = axios(config)
      .then(function (response) {
        localStorage.setItem("token", response.data.access_token);
        localStorage.setItem("userId", response.data.userId);
        return response;
      })
      .catch(function (error) {
        return error;
      });

    return requestResponse;
  },

  postAve: (ave: Ave) => {

    const config: AxiosRequestConfig = {
      method: "POST",
      url: "http://localhost:8080/aves",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      data: ave,
    };

    const requestResponse: any = axios(config)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });

    return requestResponse;
  },

  signUp: (email: string, password: string) => {

    const data = {
      email: email,
      password: password,
      roles: [
        {
          id: 1
        }
      ]
    };

    const config: AxiosRequestConfig = {
      method: "POST",
      url: "http://localhost:8080/usuarios",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    const requestResponse: any = axios(config)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });

    return requestResponse;
  },
  postAvistamento: (avistamento: { aveId: number, localizacao: string }) => {

    const data = {
      ave: { id: avistamento.aveId },
      usuario: localStorage.getItem('userId'),
      localizacao: avistamento.localizacao
    }

    const config: AxiosRequestConfig = {
      method: "POST",
      url: "http://localhost:8080/avistamentos",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      data: data,
    };

    const requestResponse: any = axios(config)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });

    return requestResponse;
  }
});