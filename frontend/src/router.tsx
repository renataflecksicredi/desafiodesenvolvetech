import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import CadastroAve from "./pages/Dashboard/CadastroAve";
import CadastroAvistamento from "./pages/Dashboard/CadastroAvistamento";
import CatalogoDeAves from "./pages/Dashboard/CatalogoDeAves";
import ConsultarAvistamentos from "./pages/Dashboard/ConsultarAvistamentos";
import Welcome from "./pages/Dashboard/Welcome";
import Inicio from "./pages/Inicio";
import CadastroUsuario from "./pages/Inicio/CadastroUsuario";
import Login from "./pages/Inicio/Login";

export default function Router() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Inicio />}>
                    <Route index element={<Login />}/>
                    <Route path="cadastroUsuario" element={<CadastroUsuario />} />
                    <Route path="dashboard" element={<Dashboard />}>
                        <Route index element={<Welcome />} />
                        <Route path="cadastroAve" element={<CadastroAve />} />
                        <Route path="cadastroAvistamento" element={<CadastroAvistamento />} />
                        <Route path="catalogoDeAves" element={<CatalogoDeAves />} />
                        <Route path="consultarAvistamentos" element={<ConsultarAvistamentos />} />
                    </Route>
                </Route>
            </Routes>
        </BrowserRouter >
    );
}