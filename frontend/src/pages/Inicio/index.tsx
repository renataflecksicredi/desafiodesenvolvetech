import { Outlet } from "react-router-dom";

export default function Inicio() {
    return (
        <Outlet />
    );
}
