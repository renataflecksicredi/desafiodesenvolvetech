import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Container, createTheme, CssBaseline, Paper, styled, ThemeProvider, Typography } from '@mui/material';
import { BASE_URL } from '../../../utils/request';
import axios from 'axios';
import { Ave } from '../../../types/Ave';
import { useState } from 'react';
import { useEffect } from 'react';


const theme = createTheme();

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

export default function CatalogoDeAves() {
    const [aves, setAves] = useState<Ave[]>([])

    useEffect(() => {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        }

        axios.get(`${BASE_URL}/aves?size=50`, config).then(response => {
            console.log(response.data)
            setAves(response.data.content)
        })
    },[])


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" sx={{ mb: 4 }}>
                <CssBaseline />
                <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
                    <Typography component="h1" variant="h4" align="center" mb={3}>
                        Catálogo de aves
                    </Typography>
                    <Table size="small">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>Nome</StyledTableCell>
                                <StyledTableCell>Cor predominante</StyledTableCell>
                                <StyledTableCell>Gênero</StyledTableCell>
                                <StyledTableCell>Tamanho</StyledTableCell>
                                <StyledTableCell>Família</StyledTableCell>
                                <StyledTableCell>Habitat</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {aves.map((ave) => (
                                <TableRow key={ave.id}>
                                    <TableCell>{ave.nome}</TableCell>
                                    <TableCell>{ave.corPredominante}</TableCell>
                                    <TableCell>{ave.genero}</TableCell>
                                    <TableCell>{ave.tamanho}</TableCell>
                                    <TableCell>{ave.familia}</TableCell>
                                    <TableCell>{ave.habitat}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </Container>
        </ThemeProvider>
    );
}