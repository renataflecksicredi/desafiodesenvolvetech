import { useEffect, useState, useReducer } from "react";
//Material UI
import { ThemeProvider } from "@emotion/react";
import {
  createTheme,
  Container,
  CssBaseline,
  Paper,
  Typography,
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
} from "@mui/material";
import type {} from "@mui/x-date-pickers/themeAugmentation";
//Components
import { useApi } from "../../../hooks/useApi";
import { useNavigate } from "react-router-dom";
import { Ave } from "../../../types/Ave";
import { BASE_URL } from "../../../utils/request";
import axios from "axios";

const theme = createTheme({
  components: {
    MuiDatePicker: {
      styleOverrides: {
        root: {
          backgroundColor: "red",
        },
      },
    },
  },
});

const formReducer = (state: any, event: { name: any; value: any }) => {
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function CadastroAvistamento() {
  const [aves, setAves] = useState<Ave[]>([]);
  const [formData, setFormData] = useReducer(formReducer, {});

  const api = useApi();

  const handleSubmit = async (event: { preventDefault: () => void }) => {
    const retornoCadastro = await api.postAvistamento(formData);

    if (retornoCadastro.status === 201) {
      alert("Avistamento cadastrado");
      window.location.reload();
    } else {
      alert("Erro ao cadastrar avistamento");
    }

    console.log(retornoCadastro);
  };

  function handleChange(event: any) {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  }

  useEffect(() => {
    let config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    axios.get(`${BASE_URL}/aves?size=50`, config).then((response) => {
      setAves(response.data.content);
    });
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="md" sx={{ mb: 4 }}>
        <CssBaseline />
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Typography component="h1" variant="h4" align="center" mb={5}>
            Cadastrar avistamento
          </Typography>
          <Grid container spacing={3} component="form" onSubmit={handleSubmit}>
            <Grid item xs={12} sm={12}>
              <FormControl fullWidth>
                <InputLabel>Ave</InputLabel>
                <Select onChange={handleChange} name="aveId" label="Ave">
                  {aves.map((ave) => {
                    return <MenuItem value={ave.id}>{ave.nome}</MenuItem>;
                  })}
                </Select>
              </FormControl>
            </Grid>
            {/* <Grid item xs={12} sm={4}>
                    <DataInput />
                    </Grid> */}
            <Grid item xs={12} sm={12}>
              <TextField
                id="localizacao"
                name="localizacao"
                label="Localização do avistamento"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button type="submit" variant="contained" size="large">
                Cadastrar
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
