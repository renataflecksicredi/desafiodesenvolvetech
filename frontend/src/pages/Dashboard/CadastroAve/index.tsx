import { useReducer } from "react";
// Material UI
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
} from "@mui/material";
import { useApi } from "../../../hooks/useApi";
import { useNavigate } from "react-router-dom";

const theme = createTheme();

const formReducer = (state: any, event: { name: any; value: any }) => {
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function CadastroAve() {
  const navigate = useNavigate();
  const api = useApi();
  const [formData, setFormData] = useReducer(formReducer, {});

  const handleSubmit = async (event: { preventDefault: () => void }) => {
    event.preventDefault();

    const retornoCadastro = await api.postAve(formData);
    console.log(retornoCadastro);

    if (retornoCadastro.status === 201) {
      alert("Ave cadastrada");
      window.location.reload();
    } else {
      alert("Erro ao cadastrar ave");
    }

    console.log(retornoCadastro);
  };

  function handleChange(
    event: { target: { name: any; value: any } },
    type?: string
  ) {

    if (type === "genero" || type === "tamanho") {
      setFormData({
        name: type,
        value: event.target.value,
      });
    } else {
      setFormData({
        name: event.target.name,
        value: event.target.value,
      });
    }
    
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="md" sx={{ mb: 4 }}>
        <CssBaseline />
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Typography component="h1" variant="h4" align="center" mb={3}>
            Cadastrar ave
          </Typography>
          <Grid container spacing={3} component="form" onSubmit={handleSubmit}>
            <Grid item xs={12} sm={6}>
              <TextField
                id="nome"
                name="nome"
                label="Nome"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="corPredominante"
                name="corPredominante"
                label="Cor predominante"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant="standard" fullWidth>
                <InputLabel>Gênero</InputLabel>
                <Select
                  onChange={(e) => handleChange(e, "genero")}
                  label="Gênero"
                >
                  <MenuItem value={"Femea"}>Fêmea</MenuItem>
                  <MenuItem value={"Macho"}>Macho</MenuItem>
                  <MenuItem value={"Desconhecido"}>Desconhecido</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant="standard" fullWidth>
                <InputLabel>Tamanho</InputLabel>
                <Select
                  onChange={(e) => handleChange(e, "tamanho")}
                  label="Tamanho"
                >
                  <MenuItem value={"Pequena"}>Pequena</MenuItem>
                  <MenuItem value={"Média"}>Média</MenuItem>
                  <MenuItem value={"Grande"}>Grande</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="familia"
                name="familia"
                label="Família"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="habitat"
                name="habitat"
                label="Habitat"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Typography variant="body1" component="h2">
                Localização da ave no guia:
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                id="pagina"
                name="pagina"
                label="Página"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                id="linha"
                name="linha"
                label="Linha"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                id="coluna"
                name="coluna"
                label="Coluna"
                fullWidth
                variant="standard"
                onChange={handleChange}
              />
            </Grid>
            <Grid
              item
              xs={12}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                type="submit"
                variant="contained"
                size="large"
                onClick={handleSubmit}
              >
                Cadastrar
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
