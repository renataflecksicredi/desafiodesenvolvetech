// Material UI
import { Box, Typography } from "@mui/material";
// Icons
import FavoriteIcon from '@mui/icons-material/Favorite';

export default function Welcome() {
    return (
        <Box sx={{ padding: '100px' }}>
            <Typography variant="h2" component="h1">
                Bem-vindo ao sistema de apoio aos Ornitólogos Amadores
                <FavoriteIcon sx={{ ml: '10px' }} />
            </Typography>
            <Typography variant="h5" component="h2" mt="30px">
                Selecione a opção desejada no menu ao lado
            </Typography>
        </Box>
    );
}