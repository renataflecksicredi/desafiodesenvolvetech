import { Box, Typography } from "@mui/material";
import { Avistamento } from "../../../types/Avistamento";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function ModalAve(props: { avistamento: Avistamento }) {
  return (
    <Box sx={style}>
      <Typography variant="h6" component="h1" sx={{ fontWeight: "bold" }}>
        Informações da ave:
      </Typography>

      <Typography variant="h6" component="h1">
        Nome: {props.avistamento.ave.nome}{" "}
      </Typography>
      <Typography variant="h6" component="h1">
        Cor predominante: {props.avistamento.ave.corPredominante}
      </Typography>
      <Typography variant="h6" component="h1">
        Gênero: {props.avistamento.ave.genero}
      </Typography>
      <Typography variant="h6" component="h1">
        Tamanho: {props.avistamento.ave.tamanho}
      </Typography>
      <Typography variant="h6" component="h1">
        Família: {props.avistamento.ave.familia}
      </Typography>
      <Typography variant="h6" component="h1">
        Habitat: {props.avistamento.ave.habitat}
      </Typography>
      <Typography
        variant="h6"
        component="h1"
        mt={2}
        sx={{ fontWeight: "bold" }}
      >
        Localização da ave no guia:
      </Typography>
      <Typography variant="h6" component="h1">
        Página: {props.avistamento.ave.pagina}
      </Typography>
      <Typography variant="h6" component="h1">
        Linha: {props.avistamento.ave.linha}
      </Typography>
      <Typography variant="h6" component="h1">
        Coluna: {props.avistamento.ave.coluna}
      </Typography>
    </Box>
  );
}