import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import {
    AvatarPropsVariantOverrides,
  Container,
  createTheme,
  CssBaseline,
  Link,
  Modal,
  Paper,
  styled,
  ThemeProvider,
  Typography,
} from "@mui/material";
import ModalAve from "../ModalAve";
import { Avistamento } from "../../../types/Avistamento";
import { useEffect, useState } from "react";
import axios from "axios";
import { BASE_URL } from "../../../utils/request";
import { Ave } from "../../../types/Ave";

const theme = createTheme();

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

export default function ConsultarAvistamentos() {

  const [avistamentos, setAvistamentos] = useState<Avistamento[]>([]);
  const [avistamento, setAvistamento] = useState<Avistamento>();

  useEffect(() => {
    let config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    axios
      .get(`${BASE_URL}/avistamentos`, config)
      .then((response) => {
        setAvistamentos(response.data.content);
      });
  }, []);

  const [open, setOpen] = React.useState(false);

  const handleOpen = (event: any, avistamento: Avistamento) => {
    setOpen(true);
    setAvistamento(avistamento);
  };

  const handleClose = () => setOpen(false);

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" sx={{ mb: 4 }}>
        <CssBaseline />
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Typography component="h1" variant="h4" align="center" mb={3}>
            Avistamentos
          </Typography>
          <Table size="small">
            <TableHead>
              <TableRow>
                <StyledTableCell>Ave avistada</StyledTableCell>
                <StyledTableCell>Data</StyledTableCell>
                <StyledTableCell>Local</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {avistamentos.map((avistamento) => (
                <TableRow key={avistamento.id}>
                  <TableCell>
                    <Link onClick={(e) => handleOpen(e, avistamento)}>
                      {avistamento.ave.nome}
                    </Link>
                  </TableCell>
                  <TableCell>{new Date(avistamento.createdAt).toLocaleDateString()}</TableCell>
                  <TableCell>{avistamento.localizacao}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {avistamento && (
            <Modal open={open} onClose={handleClose}>
              <ModalAve avistamento={avistamento}/>
            </Modal>
          )}
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
