import React from "react";
// Material UI
import { Divider, ListItemButton, ListItemIcon, ListItemText, ListSubheader } from "@mui/material";
// Icons
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
import SearchIcon from '@mui/icons-material/Search';
import AddCircleIcon from '@mui/icons-material/AddCircle';

export default function ListItems() {
    function limpaStrorage(event: any) {
        localStorage.clear();
    }

    
    return (
        <React.Fragment>

            <ListItemButton href="/dashboard">
                <ListItemIcon>
                    <HomeIcon />
                </ListItemIcon>
                <ListItemText primary="Home" />
            </ListItemButton>

            <ListItemButton href="/dashboard/cadastroAve">
                <ListItemIcon>
                    <AddCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Cadastrar ave" />
            </ListItemButton>

            <ListItemButton href='/dashboard/cadastroAvistamento'>
                <ListItemIcon>
                    <AddCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Cadastrar avistamento" />
            </ListItemButton>

            <ListItemButton href="/dashboard/catalogoDeAves">
                <ListItemIcon>
                    <SearchIcon />
                </ListItemIcon>
                <ListItemText primary="Catálogo de aves" />
            </ListItemButton>

            <ListItemButton href="/dashboard/consultarAvistamentos">
                <ListItemIcon>
                    <SearchIcon />
                </ListItemIcon>
                <ListItemText primary="Consultar avistamentos" />
            </ListItemButton>

            <ListItemButton href="/">
                <ListItemIcon>
                    <LogoutIcon />
                </ListItemIcon>
                <ListItemText primary="Sair"
                onClick={limpaStrorage} />
            </ListItemButton>

        </React.Fragment >


    );
}